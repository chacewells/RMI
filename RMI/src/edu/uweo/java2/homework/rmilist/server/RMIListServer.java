package edu.uweo.java2.homework.rmilist.server;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class RMIListServer extends UnicastRemoteObject
		implements RMIFilteredList, Runnable {
	public static final String RMI_NAME = "rmi:rmilist";
	public final String host;
	public final int port;

	public RMIListServer() throws RemoteException {
		host = "localhost";
		port = Registry.REGISTRY_PORT;
	}
	
	public RMIListServer(String regHost, int regPort) throws RemoteException {
		host = regHost;
		port = regPort;
	}

	@Override
	public <T extends Comparable<?>> List<T> getList(Collection<T> input,
			ListFilter<T> filter) throws RemoteException {
		List<T> output = new LinkedList<>();
		
		for (T item : input) {
			if ( filter.test(item) ) {
				output.add(item);
			}
		}
		
		return output;
	}
	
	@Override
	public void run() {
		try {
			Registry registry = LocateRegistry.getRegistry(host, port);
			registry.rebind(RMI_NAME, this);
		} catch (RemoteException e) {
			System.err.println(e.getMessage());
		}
	}

}
