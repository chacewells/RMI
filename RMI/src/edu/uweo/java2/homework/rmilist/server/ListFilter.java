package edu.uweo.java2.homework.rmilist.server;

import java.io.Serializable;

public interface ListFilter<T extends Comparable<?>> 
	extends Serializable {
	boolean test(T value);
}
