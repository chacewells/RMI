package com.wellsfromwales.rmi.shoes.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.NotBoundException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import com.wellsfromwales.rmi.shoes.server.ShoeCatalog;

public class ShoeCatalogClient {
	public static void main(String[] args) {
		new ShoeCatalogClient().execute();
	}
	
	public void execute() {
		Registry registry;
		try (BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in))) {
			System.out.println("getting registry");
			registry = LocateRegistry.getRegistry();
			System.out.println("getting catalog");
			ShoeCatalog rShoeCatalog = (ShoeCatalog) registry.lookup(ShoeCatalog.RMI_NAME);
			
			System.out.println("Type shoe names here to get prices (or 'quit' to quit):");
			System.out.flush();
			String line = null;
			while ( (line = keyboard.readLine()) != null ) {
				if (line.equalsIgnoreCase("quit")) {
					break;
				}
				double price = rShoeCatalog.getPrice(line);
				System.out.format("%s: $%s%n", line, price > 0 ? String.format("%.2f", price) : "shoe not found");
				System.out.flush();
			}
			System.out.println("goodbye!");
		} catch (NotBoundException | IOException e) {
			e.printStackTrace();
		}
		
	}
}
