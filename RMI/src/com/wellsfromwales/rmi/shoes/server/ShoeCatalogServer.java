package com.wellsfromwales.rmi.shoes.server;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class ShoeCatalogServer {

	public static void main(String[] args) {
		new ShoeCatalogServer().execute();
	}
	
	public void execute() {
		try {
			Registry registry = LocateRegistry.createRegistry(Registry.REGISTRY_PORT);
			
			ShoeCatalogImpl shoeCatalog = new ShoeCatalogImpl();
			ShoeCatalog remoteShoeCatalog =
					(ShoeCatalog) UnicastRemoteObject.exportObject(shoeCatalog, 0);
			
			System.out.println(registry);
			registry.rebind(ShoeCatalog.RMI_NAME, remoteShoeCatalog);
		} catch (RemoteException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

}
