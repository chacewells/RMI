package com.wellsfromwales.rmi.shoes.server;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;

public class ShoeCatalogImpl implements ShoeCatalog {
	private Map<String, Double> shoesPrices = new HashMap<>();
	{
		shoesPrices.put("Saucony Bullet", 50.0);
		shoesPrices.put("Camper", 145.0);
		shoesPrices.put("Thorocraft Porter", 160.);
		shoesPrices.put("Thorocraft Hutchinson", 170.);
	}
	
	@Override
	public double getPrice(String name) throws RemoteException {
		try {
			return shoesPrices.get(name).doubleValue();
		} catch (NullPointerException e) {
			return -1.;
		}
	}
	
	
}
