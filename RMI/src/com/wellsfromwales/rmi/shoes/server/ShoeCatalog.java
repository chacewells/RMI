package com.wellsfromwales.rmi.shoes.server;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ShoeCatalog extends Remote {
	public static final String RMI_NAME = "rmi:shoe_catalog";
	double getPrice(String name) throws RemoteException;
}
